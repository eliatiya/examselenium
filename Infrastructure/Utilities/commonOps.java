package Utilities;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class commonOps extends base
{
	@BeforeClass
	  public void beforClass() throws ParserConfigurationException, SAXException, IOException {
      initBrowser("chrome");  
	  }	
	@AfterTest
	private void afterTest() {
		driver.quit();
	}
	@AfterClass
	  public void afterClass() {
		driver.quit();
	  }
	public static String getData(String nodeName) throws ParserConfigurationException, SAXException, IOException
	{
	File fXmlFile = new File("./ConfigurationProject.xml");
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile); 
	doc.getDocumentElement().normalize();
	return doc.getElementsByTagName(nodeName).item(0).getTextContent();
	}
	
	public static void initBrowser(String browserType) throws ParserConfigurationException, SAXException, IOException
	{
		switch(browserType.toLowerCase())
		{
		case "chrome":
		driver = initChromeDriver();
		break;
		}
		driver.manage().window().maximize();
        driver.get(getData("url"));
	    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	}
	public static WebDriver initChromeDriver() throws ParserConfigurationException, SAXException, IOException
	{
		System.setProperty("webdriver.chrome.driver",getData("ChromeDriverPath"));
		WebDriver driver = new ChromeDriver();
		return driver;
	}


}
