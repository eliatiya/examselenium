package Tests;
import static org.testng.Assert.assertEquals;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import Utilities.commonOps;
public class examTests extends commonOps {
  	@Test
	public void Test1_url() throws IOException, ParserConfigurationException, SAXException
  	{ 		
  		assertEquals(driver.getCurrentUrl(), "https://www.ynet.co.il/home/0,7340,L-8,00.html");

	}
  	@Test
  	public void Test2_change_weather_city() {
  		
  		String degreeNumber  = driver.findElement(By.className("weathertempsdiv")).getText();
  		System.out.println(degreeNumber);
  		
  		Select select = new Select(driver.findElement(By.id("weathercityselect")));
        WebElement o = select.getFirstSelectedOption();
        String selectedoption = o.getText();
        System.out.println("Selected element: " + selectedoption);
        
        select.selectByValue("2");
  		String degreeNumberS  = driver.findElement(By.className("weathertempsdiv")).getText();
  		System.out.println(degreeNumberS);

	}
  	@Test
  	public void Test3_resulation_smaller() throws ParserConfigurationException, SAXException, IOException {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=800,480");

        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(ChromeOptions.CAPABILITY, options);

        //this will open chrome with set size
        WebDriver driver = new ChromeDriver();

        driver.get(getData("url"));
        driver.close();
	}
  	
  	@Test
  	public void Test4_send_friend() {
  		driver.findElement(By.className("slotSubTitle")).click();
  		driver.findElement(By.cssSelector("button.shareIcon1280.fb")).isDisplayed();
	}

}
